from webob import Request, Response
from webob import exc


def rest_controller(cls):
    def replacement(environ, start_response):
        req = Request(environ)
        try:
            instance = cls(req, **req.urlvars)
            action = req.urlvars.get('action')
            if action:
                action += '_' + req.method.lower()
            else:
                action = req.method.lower()
            try:
                method = getattr(instance, action)
            except AttributeError:
                raise exc.HTTPNotFound()
            resp = method()
            if isinstance(resp, str):
                resp = Response(text=resp)
        except exc.HTTPException as e:
            resp = e
        return resp(environ, start_response)
    return replacement
