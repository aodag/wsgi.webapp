import pytest


class Test_rest_controller:
    @pytest.fixture
    def target(self):
        from wsgi.webapp.controller import rest_controller
        return rest_controller

    def test_exception(self, target):
        from webob.exc import HTTPFound

        class cls:
            def __init__(self, request):
                self.request = request

            def get(self):
                return HTTPFound(location='/')

        app = target(cls)
        from webob import Request
        request = Request.blank('/')
        result = request.get_response(app)
        assert result.status_int == 302

    def test_no_action(self, target):
        class cls:
            def __init__(self, request):
                self.request = request

        app = target(cls)
        from webob import Request
        request = Request.blank('/')
        result = request.get_response(app)
        assert result.status_int == 404

    def test_action(self, target):
        class cls:
            def __init__(self, request, action):
                self.request = request

            def some_action_get(self):
                return 'some action'

        app = target(cls)
        from webob import Request
        request = Request.blank('/')
        request.urlvars['action'] = 'some_action'
        result = request.get_response(app)
        assert result.status_int == 200
        assert result.text == 'some action'

    def test_post(self, target):
        class cls:
            def __init__(self, request, action):
                self.request = request

            def some_action_post(self):
                return 'some action'

        app = target(cls)
        from webob import Request
        request = Request.blank('/', method="POST")
        request.urlvars['action'] = 'some_action'
        result = request.get_response(app)
        assert result.status_int == 200
        assert result.text == 'some action'
