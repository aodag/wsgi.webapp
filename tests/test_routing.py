import pytest


class Test_template_to_regex:
    @pytest.fixture
    def target(self):
        from wsgi.webapp.routing import template_to_regex
        return template_to_regex

    @pytest.mark.parametrize(
        "template,regex",
        [
            ("", "^$"),
            ("/", "^\/$"),
            ("/a", "^\/a$"),
            ("/{username}", "^\/(?P<username>[^/]+)$"),
            (r"/{username:\w+}", r"^\/(?P<username>\w+)$"),
            (r"/{v1}/{v2}", r"^\/(?P<v1>[^/]+)\/(?P<v2>[^/]+)$"),
        ])
    def test_it(self, target, template, regex):
        result = target(template)
        assert result == regex


class TestRouter:
    @pytest.fixture
    def target(self):
        from wsgi.webapp.routing import Router
        return Router

    def test_notfound(self, target):
        from webob import Request
        request = Request.blank('/')
        app = target()
        result = request.get_response(app)
        assert result.status_int == 404

    def test_it(self, target):
        from webob import Request

        request = Request.blank('/aodag')
        app = target()
        app.add_route('/{username}', __name__ + ':dummy_func')
        result = request.get_response(app)
        assert result.status_int == 200
        assert result.text == "{'username': 'aodag'}"


def dummy_func(environ, start_response):
    from webob import Request, Response
    request = Request(environ)
    return Response(text=str(request.urlvars))(environ, start_response)
